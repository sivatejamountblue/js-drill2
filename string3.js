function getMonth(date) {
    if (!date){
        return []
    }
    let result = parseInt(date.slice(0,2));

    if (result === 01) {
        console.log("January");
    }
    else if (result === 02) {
        console.log("February");
    }
    else if (result === 03) {
        console.log("March");
    }
    else if (result === 04) {
        console.log("April")
    }
    else if (result === 05) {
        console.log("May")
    }
    else if (result === 06) {
        console.log("June")
    }
    else if (result === 07) {
        console.log("July")
    }
    else if (result === 08) {
        console.log("August")
    }
    else if (result === 09) {
        console.log("September")
    }
    else if (result === 10) {
        console.log("October")
    }
    else if (result === 11) {
        console.log("November")
    }
    else if ( result === 12){
        console.log("December")
    }
    else {
        console.log([])
    }
}

module.exports = getMonth;