function fullName(object) {
    if ((!object) || (object.length === 0)){
        return []
    }
    let lowerArray = [];
    let newArray = [];
    let array = Object.values(object);
    let string = array.join(' ');
    for (let word of string.split()) {
        lowerArray.push(word.toLowerCase());
    }
    let variable = (lowerArray.join(" "))
    for (let eachWord of variable.split(" ")){
        newArray.push(eachWord[0].toUpperCase() + eachWord.slice(1))
    }
    console.log(newArray.join(' '));
    }

module.exports = fullName;